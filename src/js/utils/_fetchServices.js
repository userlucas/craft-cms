import axios from "axios";
const getData = async (url = "") => {
  try {
    let response = await axios.get(url);
    return response.data;
  } catch (Exception) {
    console.warn("Exception in getData => " + Exception);
  }
};

const get_feth = async url => {
  try {
    return await getData(url);
  } catch (Exception) {
    console.warn("Exception in getData => " + Exception);
  }
};

export { getData, get_feth };
