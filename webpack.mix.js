const mix = require("laravel-mix");
const purgecss = require("@fullhuman/postcss-purgecss");
const fs = require("fs");
const optionsApp = [];
const components = ["posterGrid"];

/*mix.webpackConfig({*/
/*stats: {*/
/*children: true*/
/*}*/
/*});*/

if (mix.inProduction()) {
  optionsApp.push(
    purgecss({
      content: ["**/*.twig", "**/*.html"],
      defaultExtractor: content => content.match(/[\w-/.:]+(?<!:)/g) || [],
      safelist: {
        deep: [
          /iziToast*/,
          /headroom*/,
          /collapse*/,
          /show*/,
          /swiper*/,
          /sweetalert2*/,
          /is-invalid*/
        ]
      }
    })
  );
}

components.forEach(component => {
  try {
    if (fs.existsSync(`./templates/components/${component}/src/style.scss`)) {
      mix.sass(
        `./templates/components/${component}/src/style.scss`,
        `./web/components/${component}/main.css`,
        {},
        optionsAp
      );
    }
    if (fs.existsSync(`./templates/components/${component}/src/index.js`)) {
      mix.js(
        `./templates/components/${component}/src/index.js`,
        `./web/components/${component}/main.js`
      );
    }
  } catch (Exception) {
    console.warn("Exception in compress => " + Exception);
  }
});

mix
  .sass("src/scss/app.scss", "web/css", {}, optionsApp)
  .minify("web/css/app.css")
  .sourceMaps();

mix
  .js("src/js/app.js", "web/js")
  .minify("web/js/app.js")
  .sourceMaps();
